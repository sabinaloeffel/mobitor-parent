package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "changelog.config")
public class ChangelogConfiguration {

    private final static Logger LOG = LoggerFactory.getLogger(ChangelogConfiguration.class);

    private String customFieldNameDeploymentEnvironments;
    private String customFieldNameDeploymentDatePreProd;
    private String customFieldNameDeploymentDateProduction;

    private List<String> blacklistedProjects;

    @PostConstruct
    public void logProperties() {
        LOG.info("Jira field-name DeploymentEnvironments:    " + customFieldNameDeploymentEnvironments);
        LOG.info("Jira field-name DeploymentDatePreProd:     " + customFieldNameDeploymentDatePreProd);
        LOG.info("Jira field-name DeploymentDateProduction:  " + customFieldNameDeploymentDateProduction);
    }

    public String getCustomFieldNameDeploymentEnvironments() {
        return customFieldNameDeploymentEnvironments;
    }

    public void setCustomFieldNameDeploymentEnvironments(String customFieldNameDeploymentEnvironments) {
        this.customFieldNameDeploymentEnvironments = customFieldNameDeploymentEnvironments;
    }

    public String getCustomFieldNameDeploymentDatePreProd() {
        return customFieldNameDeploymentDatePreProd;
    }

    public void setCustomFieldNameDeploymentDatePreProd(String customFieldNameDeploymentDatePreProd) {
        this.customFieldNameDeploymentDatePreProd = customFieldNameDeploymentDatePreProd;
    }

    public String getCustomFieldNameDeploymentDateProduction() {
        return customFieldNameDeploymentDateProduction;
    }

    public void setCustomFieldNameDeploymentDateProduction(String customFieldNameDeploymentDateProduction) {
        this.customFieldNameDeploymentDateProduction = customFieldNameDeploymentDateProduction;
    }

    public List<String> getBlacklistedProjects() {
        return blacklistedProjects;
    }

    public void setBlacklistedProjects(List<String> blacklistedProjects) {
        this.blacklistedProjects = blacklistedProjects;
    }
}
