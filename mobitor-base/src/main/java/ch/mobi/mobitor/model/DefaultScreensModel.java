package ch.mobi.mobitor.model;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class DefaultScreensModel implements ScreensModel {

    private Map<String, Screen> screens = new LinkedHashMap<>();

    @Override
    public <T extends Screen> void addScreen(T screen) {
        screens.put(screen.getConfigKey(), screen);
    }

    @Override
    public boolean hasScreen(String configKey) {
        return this.screens.containsKey(configKey);
    }

    @Override
    public <T extends Screen> T getScreen(String screenConfigKey) {
        return (T)this.screens.get(screenConfigKey);
    }

    @Override
    public <T extends Screen> List<T> getAvailableScreens() {
        List<Screen> screens = new ArrayList<>(this.screens.values());
        return (List<T>)Collections.unmodifiableList(screens);
    }

}
