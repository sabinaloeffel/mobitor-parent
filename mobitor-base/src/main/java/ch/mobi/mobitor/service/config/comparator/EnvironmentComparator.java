package ch.mobi.mobitor.service.config.comparator;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;


public class EnvironmentComparator implements Comparator<String> {

    static final List<String> ENV_KEYS_ORDER = Lists.newArrayList("build", "Y", "B", "W", "V", "Z", "I", "T", "P");

    @Override
    public int compare(String o1, String o2) {
        if (o1 == null) {
            return -1;
        } else if (o2 == null) {
            return +1;
        }

        int pos1 = ENV_KEYS_ORDER.indexOf(o1);
        int pos2 = ENV_KEYS_ORDER.indexOf(o2);

        return (pos1 - pos2);
    }
}
