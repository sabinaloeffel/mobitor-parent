package ch.mobi.mobitor.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.ChangelogConfiguration;
import ch.mobi.mobitor.domain.config.ChangelogConfig;
import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.plugin.bitbucket.service.client.BitBucketClient;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitInfoResponse;
import ch.mobi.mobitor.plugin.jira.service.client.JiraClient;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import ch.mobi.mobitor.service.DeploymentInformationService;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.config.ChangelogConfigurationService;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.plugin.jira.service.JiraPluginConstants.CACHE_NAME_CHANGELOG_ISSUES;
import static ch.mobi.mobitor.plugin.jira.service.client.JiraClient.DATETIME_FORMAT;

@Component
@ConditionalOnProperty(name = "mobitor.modules.changelog.enabled", havingValue = "true")
public class ChangelogInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(ChangelogInformationCollector.class);

    private final BitBucketClient bitBucketClient;
    private final JiraClient jiraClient;
    private final DeploymentInformationService deploymentInformationService;
    private final ChangelogConfigurationService changelogConfigurationService;
    private final EnvironmentsConfigurationService environmentsConfigurationService;
    private final ChangelogConfiguration changelogConfiguration;

    @Autowired
    public ChangelogInformationCollector(BitBucketClient bitBucketClient,
                                         JiraClient jiraClient,
                                         DeploymentInformationService deploymentInformationService,
                                         ChangelogConfigurationService changelogConfigurationService,
                                         EnvironmentsConfigurationService environmentsConfigurationService,
                                         ChangelogConfiguration changelogConfiguration) {
        this.bitBucketClient = bitBucketClient;
        this.jiraClient = jiraClient;
        this.deploymentInformationService = deploymentInformationService;
        this.changelogConfigurationService = changelogConfigurationService;
        this.environmentsConfigurationService = environmentsConfigurationService;
        this.changelogConfiguration = changelogConfiguration;
    }

    @Scheduled(cron = "0 0 5,12 * * ?") // since this collector causes quite a jira request avalanche, only run this twice a day on 5:00 and 12:00
    @CacheEvict(cacheNames = CACHE_NAME_CHANGELOG_ISSUES, allEntries = true)
    public void collectChangelogInformation() {
        long start = System.currentTimeMillis();
        changelogConfigurationService.getAllChangelogConfigs().forEach(this::loadAndForwardChangelogInformation);
        long stop = System.currentTimeMillis();
        LOG.info("reading changelog information took: " + (stop-start) + "ms");

        changelogConfigurationService.setChangelogRefresh(new Date());
    }

    private void loadAndForwardChangelogInformation(ChangelogConfig changelogConfig) {
        String serverName = changelogConfig.getServerName();
        List<String> envs = changelogConfig.getEnvironments();

        envs.forEach(env -> {
            Deployment deployment = loadSuccessfulDeployment(serverName, env);
            if (deployment != null) {
                String version = deployment.getFirstApplicationVersion();

                if (StringUtils.isNotBlank(changelogConfig.getParameterName())) {
                    version = deployment.getParameter(changelogConfig.getParameterName());
                }

                String project = changelogConfig.getProject();
                String repository = changelogConfig.getRepository();
                String until = "release/" + StringUtils.trimToEmpty(version);

                List<BitBucketCommitInfoResponse> commits = bitBucketClient.retrieveCommits(project, repository, until);
                Set<String> jiraIssues = filterIssuesInCommitsByEndingTransition(commits);
                long deploymentTimestamp = deployment.getDeploymentDate();
                for (String jiraIssueKey : jiraIssues) {
                    if (issueIsNotInBlacklistedProject(jiraIssueKey)) {
                        IssueResponse issueResponse = jiraClient.retrieveIssue(jiraIssueKey);
                        if (issueResponse != null) {
                            Set<String> deploymentEnvironments = issueResponse.getFields().getDeploymentEnvironments(changelogConfiguration.getCustomFieldNameDeploymentEnvironments());
                            String jiraEnvName = environmentsConfigurationService.getName(env);
                            HashSet<String> envsForJira = new HashSet<>(deploymentEnvironments);
                            envsForJira.add(jiraEnvName);

                            if (SetUtils.isEqualSet(deploymentEnvironments, envsForJira)) {
                                LOG.debug("Issue does not require updating, no changes: " + jiraIssueKey);
                            } else {
                                jiraClient.updateCustomField(jiraIssueKey, changelogConfiguration.getCustomFieldNameDeploymentEnvironments(), envsForJira);
                                LOG.info("Updated Jira issue: " + jiraIssueKey);
                            }

                            // check if envsForJira contains prod or preprod environments, if so update the deployment date field:
                            /* TODO the "preprod-t" and "produktion-p" strings refer to \src\main\resources\screen\environment-mappings.json
                                    not sure if the customfield name from jira should be in there? or some mapping config which environment updates
                                    which jira field??
                            */
                            if ("preprod-t".equals(jiraEnvName)) {
                                updateDeploymentDate(jiraIssueKey, issueResponse, changelogConfiguration.getCustomFieldNameDeploymentDatePreProd(), deploymentTimestamp);
                            }
                            if ("produktion-p".equals(jiraEnvName)) {
                                updateDeploymentDate(jiraIssueKey, issueResponse, changelogConfiguration.getCustomFieldNameDeploymentDateProduction(), deploymentTimestamp);
                            }

                        } else {
                            LOG.warn("Commit message contains non-existing issue: " + jiraIssueKey);
                        }
                    }
                }
            }
        });
    }

    public boolean issueIsNotInBlacklistedProject(String jiraIssueKey) {
        for (String blacklistedProjectPrefix : changelogConfiguration.getBlacklistedProjects()) {
            if (jiraIssueKey.startsWith(blacklistedProjectPrefix)) {
                return false;
            }
        }
        return true;
    }

    private void updateDeploymentDate(String jiraIssueKey, IssueResponse issueResponse, String customFieldName, long deploymentTimestamp) {
        Date deploymentDate = issueResponse.getFields().getDeploymentDate(customFieldName);
        if (deploymentDate == null) {
            String currentDateForJira = DateTimeFormat.forPattern(DATETIME_FORMAT).print(deploymentTimestamp);
            jiraClient.updateCustomField(jiraIssueKey, customFieldName, currentDateForJira);
        }
    }

    private Set<String> filterIssuesInCommitsByEndingTransition(List<BitBucketCommitInfoResponse> commits) {
        Set<String> jiraIssueSet = commits.stream().map(BitBucketCommitInfoResponse::getJiraKeys).flatMap(List::stream).collect(Collectors.toSet());
        LOG.debug("Found Jira Issues: " + Objects.toString(jiraIssueSet));

        return jiraIssueSet;
    }

    private Deployment loadSuccessfulDeployment(String serverName, String environment) {
        try {
            List<Deployment> deployments = deploymentInformationService.retrieveDeployments(serverName);
            List<Deployment> filteredDeployments = deploymentInformationService.filterDeployments(deployments, Sets.newHashSet(environment));
            if (filteredDeployments != null && filteredDeployments.size() == 1) {
                Deployment deployment = filteredDeployments.get(0);
                return deployment.isSuccessful() ? deployment : null;
            }

        } catch (Exception e) {
            LOG.error("Could not load deployment from LIIMA.");
        }

        return null;
    }

}
