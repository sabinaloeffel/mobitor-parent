package ch.mobi.mobitor.service;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.config.OnDutyConfig;
import org.joda.time.DateTime;
import org.joda.time.Weeks;
import org.springframework.stereotype.Service;

@Service
public class OnDutyService {

    public String getCurrentOnDutyTeam(OnDutyConfig onDuty) {
        int weeks = getWeeksUntilNow(onDuty.getSprintStart());
        int sprintNumbers = weeks / onDuty.getSprintDuration();
        int teamOnDuty = (sprintNumbers % onDuty.getTeams().size());
        return onDuty.getTeams().get(teamOnDuty);
    }

    public String getNextOnDutyTeam(OnDutyConfig onDuty){
        int nextPosition = onDuty.getTeams().indexOf(getCurrentOnDutyTeam(onDuty))+1;
        if(nextPosition > onDuty.getTeams().size()-1){
            nextPosition = 0;
        }
        return onDuty.getTeams().get(nextPosition);
    }

    public String getChangeDate(OnDutyConfig onDuty){
        int weeks = getWeeksUntilNow(onDuty.getSprintStart());
        if (0 == weeks%onDuty.getSprintDuration()){
            weeks = onDuty.getSprintDuration() + weeks;
        } else {
            weeks = weeks%onDuty.getSprintDuration() + weeks;
        }
        DateTime changeDate = stringToDateTime(onDuty.getSprintStart()).minusDays(1).plusWeeks(weeks);
        return changeDate.toString("dd-MM-yyyy");
    }

    public int getWeeksUntilNow(String sprintStart) {

        return Weeks.weeksBetween(stringToDateTime(sprintStart), getNow()).getWeeks();
    }

    public DateTime stringToDateTime(String time){
        return new DateTime(DateTime.parse(time));
    }

    public DateTime getNow(){
        return new DateTime();
    }
}
