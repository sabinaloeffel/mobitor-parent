package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.service.config.ChangelogConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ChangelogController {

    private final ChangelogConfigurationService changelogConfigurationService;
    private final ChangelogConfiguration changelogConfiguration;

    @Autowired
    public ChangelogController(ChangelogConfigurationService changelogConfigurationService,
                               ChangelogConfiguration changelogConfiguration) {
        this.changelogConfigurationService = changelogConfigurationService;
        this.changelogConfiguration = changelogConfiguration;
    }

    @RequestMapping("/changelog")
    public String overview(Model model) {

        model.addAttribute("changelogRefresh", changelogConfigurationService.getChangelogRefresh());
        model.addAttribute("changelogConfigs", changelogConfigurationService.getAllChangelogConfigs());
        model.addAttribute("blacklistedProjects", changelogConfiguration.getBlacklistedProjects());

        return "changelog";
    }

}
