package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import java.util.List;
import java.util.Set;

public class DefaultRuleEvaluation implements RuleEvaluation {

    /**
     * environment ("Y", "W", "X") is used as key for both multi maps
     */
    private ListMultimap<String, ApplicationInformation> violationsMap = ArrayListMultimap.create();
    private ListMultimap<String, RuleMessage> messageMap = ArrayListMultimap.create();

    public void addViolation(String env, ApplicationInformation applicationInformation, RuleViolationSeverity severity, String message) {
        violationsMap.put(env, applicationInformation);
        messageMap.put(env, new RuleMessage(severity, message));
    }

    public boolean hasErrors() {
        return hasMessageWithSeverity(RuleViolationSeverity.ERROR);
    }

    @Override
    public boolean hasWarnings() {
        return hasMessageWithSeverity(RuleViolationSeverity.WARNING);
    }

    public boolean hasViolationsOrWarnings() {
        return hasErrors() || hasWarnings();
    }

    private boolean hasMessageWithSeverity(RuleViolationSeverity severity) {
        for (RuleMessage ruleMessage : this.messageMap.values()) {
            if (severity.equals(ruleMessage.getSeverity())) {
                return true;
            }
        }
        return false;
    }

    public Set<String> getAffectedEnvironments() {
        return violationsMap.keySet();
    }

    public List<RuleMessage> getMessages(String environment) {
        return this.messageMap.get(environment);
    }
}
