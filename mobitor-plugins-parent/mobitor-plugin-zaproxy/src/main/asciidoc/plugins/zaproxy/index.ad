= Mobitor ZA Proxy Plugin

link:../../index.html[Back Mobitor Overview]

== Features

Zaproxy is a penenatration testing tool which checks for vulnerabilities in web applications and web services. During the build in TeamCity, it generates a report which is uploaded to SonarQube. In this application, it is possible to manage these issues (e.g. define an issue to be false positive). The managed state in SonarQube is then shown on the Mobitor.

https://github.com/zaproxy/zaproxy[zaproxy] is (Zed Attack Proxy - ZAP) is one of the world’s most popular free security
tools.


== Configuration

.screen.json
[source, json]
----
  "zaproxyApplications": [
    {
      "projectKey": "sonarqube:projectkey:pentest",
      "serverName": "server_name_1",
      "applicationName": "application_name_1",
      "environment": "build"
    }
  ]
----

.Parameters
|===
| Name | Description | Required | Default

| projectKey
| The projectKey of the SonarQube project where the Zaproxy report is uploaded. projectKey ends most of the time with :pentest.
| yes
| empty

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationName
| The application name that refers to the sub-row of the server name in the same screen. Used to reference the position if this information block in the screen
| yes
| empty

| environment
| Environment of this information block. Refers to the environments in the same screen. Used to reference the position in the screen (the column).
| yes
| empty
|===


== Result

image::za-proxy-information-block.png[]

The flag indicates if the Zaproxy report contains any detected vulnerabilities.

