package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RestCallsTkNameIdPatternRuleTest extends PipelineRuleTest<RestCallsTkNameIdPatternRule> {

    @Test
    public void evaluateRuleWithValidTkNameId() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        restInfo.setTkNameId("aid-fknameid-service");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors()).isFalse();
    }

    @Test
    public void evaluateRuleWithInvalidTkNameId() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        restInfo.setTkNameId("name");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors()).isTrue();
    }

    @Test
    public void validatesType() {
        RestCallsTkNameIdPatternRule rule = new RestCallsTkNameIdPatternRule();

        assertThat(rule.validatesType(REST)).isTrue();
    }

    @Test
    public void ruleViolated() {
        // arrange
        // act
        // assert
        assertTrue(isValidTkNameId("diu-elan2insign-service"));
        assertTrue(isValidTkNameId("b2e-profil-service"));
        assertTrue(isValidTkNameId("vvn-baustein-service"));
        assertTrue(isValidTkNameId("vvn-baustein-rwc"));
        assertTrue(isValidTkNameId("vvn-baustein-postgresql"));
        assertTrue(isValidTkNameId("abc-def-ghij"));
        assertTrue(isValidTkNameId("abc-sdfsdgasdgfasfd-asdfasghij"));
        assertTrue(isValidTkNameId("abc-d-e"));
        assertTrue(isValidTkNameId("pdv-partnersuche-loader-service"));
        assertTrue(isValidTkNameId("pdv-partnersuche-loader2db-service"));
        assertTrue(isValidTkNameId("pdv-partner4suche-loader4spoud-webapp"));
        assertTrue(isValidTkNameId("peddv-partner4suche-loader4spoud-webapp"));
        assertTrue(isValidTkNameId("peddv-partner4suche-webapp"));
        assertTrue(isValidTkNameId("pddv-partner4suche-webapp"));

        assertFalse(isValidTkNameId("abcdef-cd-service"));
        assertFalse(isValidTkNameId("ab-cd"));
        assertFalse(isValidTkNameId("ab-cd-cd-cd"));
        assertFalse(isValidTkNameId("a"));
        assertFalse(isValidTkNameId("a_b_c"));
        assertFalse(isValidTkNameId("!/ovn/flotte"));
        assertFalse(isValidTkNameId(""));
        assertFalse(isValidTkNameId(null));

        assertTrue(isValidTkNameId("app-name-jeeservice"));
        assertTrue(isValidTkNameId("mps-core-jeeservice"));
        assertTrue(isValidTkNameId("mps-core-b2b-jsfwebclient"));
        assertTrue(isValidTkNameId("mps-core-b2e-jsfwebclient"));
        assertTrue(isValidTkNameId("mps-core-standalone-jsfwebclient"));
        assertTrue(isValidTkNameId("mps-core-siebel-jsfwebclient"));
        assertTrue(isValidTkNameId("mps-core-uid-javabatch"));
    }

    private boolean isValidTkNameId(String tkNameId) {
        RestCallsTkNameIdPatternRule tkNameIdRule = new RestCallsTkNameIdPatternRule();
        RestCallInformation restCallInfo = new RestCallInformation();
        restCallInfo.setSwaggerUri("http://swagger.local");
        restCallInfo.setTkNameId(tkNameId);

        return !tkNameIdRule.ruleViolated(restCallInfo);
    }


    @Override
    protected RestCallsTkNameIdPatternRule createNewRule() {
        return new RestCallsTkNameIdPatternRule();
    }
}
