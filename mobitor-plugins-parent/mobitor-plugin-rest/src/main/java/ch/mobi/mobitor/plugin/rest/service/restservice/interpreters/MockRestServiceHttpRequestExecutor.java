package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class MockRestServiceHttpRequestExecutor implements RestServiceHttpRequestExecutor {

    private final String json;
    private final int statusCode;

    public MockRestServiceHttpRequestExecutor(String json, int statusCode) {
        this.json = json;
        this.statusCode = statusCode;
    }

    @Override
    public HttpResponse execute(String uri) {
        StatusLine statusLine = new BasicStatusLine(new ProtocolVersion("http", 2, 0), statusCode, "reasonPhrase");
        BasicHttpResponse httpResponse = new BasicHttpResponse(statusLine);
        BasicHttpEntity entity = new BasicHttpEntity();
        InputStream stream = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
        entity.setContent(stream);
        httpResponse.setEntity(entity);

        return httpResponse;
    }
}
