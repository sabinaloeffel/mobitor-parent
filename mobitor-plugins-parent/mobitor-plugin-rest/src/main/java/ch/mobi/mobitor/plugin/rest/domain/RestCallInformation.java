package ch.mobi.mobitor.plugin.rest.domain;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RestCallInformation implements ApplicationInformation {

    public static final String REST = "rest";

    private String swaggerUri;

    private List<RestServiceResponse> restServiceResponses = new ArrayList<>();
    private Date refreshDate;
    private String serverName;
    private String environment;

    private String tkNameId;
    private String stackName;
    private List<RestCallAdditionalUri> additionalUris;
    private String stackVersion;

    public RestCallInformation() {
    }

    @Override
    public String getType() {
        return REST;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public List<RestServiceResponse> getRestServiceResponses() {
        return restServiceResponses;
    }

    public void setRestServiceResponses(List<RestServiceResponse> restServiceResponses) {
        this.restServiceResponses = restServiceResponses;
    }

    public String getSwaggerUri() {
        return swaggerUri;
    }

    public void setSwaggerUri(String swaggerUri) {
        this.swaggerUri = swaggerUri;
    }

    public boolean hasAnyErrors() {
        boolean allOk = true;
        for (RestServiceResponse response : restServiceResponses) {
            allOk = allOk && response.isSuccess();
        }

        return !allOk;
    }

    public boolean hasAnySlowCalls() {
        boolean allCallsFast = true;
        for (RestServiceResponse entry : restServiceResponses) {
                allCallsFast = allCallsFast && !entry.durationTooSlow();
        }

        return !allCallsFast;
    }

    public long getNumberOfErrors() {
        long errors = restServiceResponses.stream().filter(response -> !response.isSuccess()).count();
        return errors;
    }

    public long getNumberOfRestCalls() {
        return restServiceResponses.size();
    }

    public Date getRefreshDate() {
        return refreshDate;
    }

    public void setRefreshDate(Date refreshDate) {
        this.refreshDate = refreshDate;
    }


    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return serverName;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getTkNameId() {
        return tkNameId;
    }

    public void setTkNameId(String tkNameId) {
        this.tkNameId = tkNameId;
    }

    public void setAdditionalUris(List<RestCallAdditionalUri> additionalUris) {
        this.additionalUris = additionalUris;
    }

    public List<RestCallAdditionalUri> getAdditionalUris() {
        return additionalUris;
    }

    public void setStackName(String stackName) {
        this.stackName = stackName;
    }

    public String getStackName() {
        return stackName;
    }

    public String getStackVersion() {
        return stackVersion;
    }

    public void setStackVersion(String stackVersion) {
        this.stackVersion = stackVersion;
    }
}
