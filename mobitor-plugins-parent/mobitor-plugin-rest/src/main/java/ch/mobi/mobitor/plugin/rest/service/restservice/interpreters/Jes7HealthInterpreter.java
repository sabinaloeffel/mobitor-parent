package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.interpreters.healthstatus.HealthVerification;
import ch.mobi.mobitor.plugin.rest.domain.interpreters.healthstatus.JapHealthResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;
import static java.text.MessageFormat.format;

@Component
public class Jes7HealthInterpreter implements SwaggerEndpointInterpreter {

    private static final Logger LOG = LoggerFactory.getLogger(Jes7HealthInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public Jes7HealthInterpreter(@Qualifier("authenticating") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        Predicate<String> containsPathElement = path -> path.contains("/jap/health");
        return containsPathElement;
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            JapHealthResponse japHealthResponse = createJapHealthResponse(json);
            if (japHealthResponse == null) {
                return RestServiceResponse.createErrorRestServiceResponse(uri,-10, "Health response was null: " + uri);

            } else {
                ResponseInterpretation interpretation = japHealthResponse.isEnabled() ? SUCCESS : FAILURE;
                List<String> messages = new ArrayList<>();
                if (FAILURE == interpretation) {
                    japHealthResponse.getVerifications()
                                     .stream()
                                     .filter(HealthVerification::hasFailed)
                                     .forEach(v -> messages.add(format("Reason for failed /health of ''{0}'': {1}", v.getName(), v.getOutcome().getReason())));
                }
                RestServiceResponse restServiceResponse = new RestServiceResponse(uri, statusCode, interpretation, messages, -1);

                return restServiceResponse;
            }

        } catch (Exception e) {
            String actMsg = "Could not query uri: " + uri;
            String exMsg = ExceptionUtils.getStackTrace(e);
            LOG.error(actMsg, e);
            return RestServiceResponse.createErrorRestServiceResponse(uri,-40, actMsg, exMsg);
        }
    }

    private JapHealthResponse createJapHealthResponse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try {
            List<HealthVerification> verifications = mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, HealthVerification.class));
            JapHealthResponse healthStatusResponse = new JapHealthResponse(verifications);
            return healthStatusResponse;

        } catch (Exception e) {
            return null;
        }
    }

}
