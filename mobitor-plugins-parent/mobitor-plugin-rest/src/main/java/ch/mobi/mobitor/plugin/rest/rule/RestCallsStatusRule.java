package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;


@Component
public class RestCallsStatusRule implements PipelineRule {

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation re) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<RestCallInformation> restCallInformationList = serverContext.getMatchingInformation(REST);

            restCallInformationList.stream().filter(this::ruleViolated).forEach(restCallInformation -> re.addViolation(env, restCallInformation, RuleViolationSeverity.ERROR, "HTTP Status Code"));
        }
    }

    private boolean ruleViolated(RestCallInformation restCallInformation) {
        return restCallInformation.hasAnyErrors();
    }

    @Override
    public boolean validatesType(String type) {
        return REST.equals(type);
    }
}
