package ch.mobi.mobitor.plugin.rest.service.scheduling;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.service.scheduling.domain.BeatLogEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Component
public class RestCallMetricService {

    private final static Logger LOG = LoggerFactory.getLogger(RestCallMetricService.class);

    private final MeterRegistry meterRegistry;

    private ObjectMapper om = new ObjectMapper();

    // prevent loosing gauges due to GC:
    // https://micrometer.io/docs/concepts#_why_is_my_gauge_reporting_nan_or_disappearing
    // https://github.com/micrometer-metrics/micrometer-docs/issues/23 (follow commit link)
    /** map to keep references to the monitored gauges, gauge must monitor a set-able object */
    private Map<List<Tag>, AtomicLong> restErrorFailuresMap = new HashMap<>();

    @Autowired
    public RestCallMetricService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void submitRestCallFailures(@NotNull RestCallInformation restCallInformation) {
        String serverName = restCallInformation.getServerName();
        String environment = restCallInformation.getEnvironment();
        long failures = restCallInformation.getNumberOfErrors();

        Tag serverNameTag = Tag.of("server", serverName);
        Tag envTag = Tag.of("env", environment);
        List<Tag> tags = Arrays.asList(serverNameTag, envTag);

        if (restErrorFailuresMap.containsKey(tags)) {
            AtomicLong failuresForGauge = restErrorFailuresMap.get(tags);
            failuresForGauge.set(failures);
        } else {
            AtomicLong atomicFailuresReference = meterRegistry.gauge("gauge_rest_status_failures", tags, new AtomicLong(failures));
            restErrorFailuresMap.put(tags, atomicFailuresReference);
        }

        writeHeartBeatLog(restCallInformation);
    }

    /**
     * LOG this in JSON Format for Spoud Analytics
     */
    void writeHeartBeatLog(@NotNull RestCallInformation restCallInformation) {
        if (isNotEmpty(restCallInformation.getSwaggerUri())) {
            double failures = restCallInformation.getNumberOfErrors();
            DateTimeFormatter eventTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
            String eventTime = eventTimeFormatter.format(ZonedDateTime.ofInstant(restCallInformation.getRefreshDate().toInstant(), ZoneId.systemDefault()));

            BeatLogEntry logEntry = new BeatLogEntry();
            logEntry.setBeatType("HealthStatusBeat");
            logEntry.setHealthStatusType("Shakedown");
            logEntry.setTkNameId(restCallInformation.getTkNameId());
            logEntry.setServerName(restCallInformation.getServerName());
            logEntry.setStage(restCallInformation.getEnvironment());
            logEntry.setStatus(failures > 0 ? "error" : "success");
            logEntry.setErrorCount(failures);
            logEntry.setEventTime(eventTime);

            try {
                String logJson = om.writeValueAsString(logEntry);

                LOG.info("HealthStatusBeat: " + logJson);
            } catch (JsonProcessingException e) {
                LOG.warn("Could not write BeatLog.");
            }
        }
    }

}
