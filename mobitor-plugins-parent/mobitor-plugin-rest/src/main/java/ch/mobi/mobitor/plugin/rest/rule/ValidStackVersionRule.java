package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;


@Component
public class ValidStackVersionRule implements PipelineRule {

    private final List<ValidStackVersion> validStackVersionEvaluators;

    @Autowired
    public ValidStackVersionRule(List<ValidStackVersion> validStackVersionEvaluators) {
        this.validStackVersionEvaluators = validStackVersionEvaluators;
    }

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation ruleEvaluation) {
        for (ValidStackVersion validStackVersionEvaluator : validStackVersionEvaluators) {
            evaluate(pipeline, ruleEvaluation, validStackVersionEvaluator);
        }
    }

    private void evaluate(Pipeline pipeline, RuleEvaluation ruleEvaluation, ValidStackVersion validStackVersionEvaluator) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> serverContextEntry : serverContextMap.entrySet()) {
            String environment = serverContextEntry.getKey();
            ServerContext serverContext = serverContextEntry.getValue();
            List<RestCallInformation> restCallInformationList = serverContext.getMatchingInformation(REST);
            for (RestCallInformation restCallInformation : restCallInformationList) {
                String stackName = restCallInformation.getStackName();
                if (stackName != null) {
                    String stackVersion = restCallInformation.getStackVersion();
                    Optional<StackVersionViolation> stackVersionViolationOptional = validStackVersionEvaluator.check(stackName, stackVersion);
                    if (stackVersionViolationOptional.isPresent()) {
                        StackVersionViolation violation = stackVersionViolationOptional.get();
                        RuleViolationSeverity severity = violation.getSeverity();
                        String message = violation.getMessage();
                        ruleEvaluation.addViolation(environment, restCallInformation, severity, message);
                    }
                }
            }
        }
    }


    @Override
    public boolean validatesType(String type) {
        return REST.equals(type);
    }
}
