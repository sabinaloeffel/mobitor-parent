package ch.mobi.mobitor.plugin.rest.domain;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;

public class FlywayRestServiceResponse extends RestServiceResponse {

    private String version;
    private String script;
    private String state;
    private Date installedOn;

    public FlywayRestServiceResponse(String path, int statusCode, ResponseInterpretation interpretation, List<String> messages, long durationMs) {
        super(path, statusCode, interpretation, messages, durationMs);
    }

    public static FlywayRestServiceResponse fromRestServiceResponses(List<RestServiceResponse> restServiceResponses) {
        return restServiceResponses.stream()
                                   .filter(resp -> resp instanceof FlywayRestServiceResponse)
                                   .map(e -> (FlywayRestServiceResponse) e)
                                   .findFirst()
                                   .orElse(null);
    }

    public static FlywayRestServiceResponse createErrorRestServiceResponse(String path, int statusCode) {
        return new FlywayRestServiceResponse(path, statusCode, FAILURE, new ArrayList<>(), -1);
    }

    public static FlywayRestServiceResponse createSuccessRestServiceResponse(String path, int statusCode, long durationMs) {
        FlywayRestServiceResponse flywayRestServiceResponse = new FlywayRestServiceResponse(path, statusCode, SUCCESS, new ArrayList<>(), durationMs);
        flywayRestServiceResponse.setState("SUCCESS");
        return flywayRestServiceResponse;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getInstalledOn() {
        return installedOn;
    }

    public void setInstalledOn(Date installedOn) {
        this.installedOn = installedOn;
    }

    public boolean hasAnyErrors() {
        return !"SUCCESS".equals(state) && !"FUTURE_SUCCESS".equals(state);
    }

    @Override
    public boolean isSuccess() {
        return super.isSuccess() && !hasAnyErrors();
    }
}
