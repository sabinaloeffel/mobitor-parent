package ch.mobi.mobitor.plugin.sonarqube;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.sonarqube.config.SonarProjectConfig;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.sonarqube.service.SonarQubeAttributeProvider;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ScreenAttributeProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.sonarProjects.enabled", havingValue = "true")
public class SonarQubePlugin implements MobitorPlugin<SonarProjectConfig> {

    private final SonarQubeAttributeProvider sonarQubeAttributeProvider;

    @Autowired
    public SonarQubePlugin(SonarQubeAttributeProvider sonarQubeAttributeProvider) {
        this.sonarQubeAttributeProvider = sonarQubeAttributeProvider;
    }

    @Override
    public String getConfigPropertyName() {
        return "sonarProjects";
    }

    @Override
    public Class<SonarProjectConfig> getConfigClass() {
        return SonarProjectConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<SonarProjectConfig> configs) {
        for (SonarProjectConfig sonarProjectConfig : configs) {
            String serverName = sonarProjectConfig.getServerName();
            String applicationName = sonarProjectConfig.getApplicationName();
            String environment = sonarProjectConfig.getEnvironment();
            String projectKey = sonarProjectConfig.getProjectKey();
            boolean strictViolations = sonarProjectConfig.getStrictViolations();

            SonarInformation sonarInformation = new SonarInformation(projectKey, strictViolations);
            sonarInformation.setCoverageLimit(sonarProjectConfig.getCoverageLimit());
            sonarInformation.setBlockerLimit(sonarProjectConfig.getBlockerLimit());
            sonarInformation.setCriticalLimit(sonarProjectConfig.getCriticalLimit());
            screen.addInformation(serverName, applicationName, environment, sonarInformation);
        }
    }

    @Override
    public ScreenAttributeProvider getScreenAttributeProvider() {
        return sonarQubeAttributeProvider;
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new SonarQubeLegendGenerator().getLegendList();
    }

}
