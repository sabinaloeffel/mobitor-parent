package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class CoverageMetricService {

    private final MeterRegistry meterRegistry;

    // prevent loosing gauges due to GC:
    // https://micrometer.io/docs/concepts#_why_is_my_gauge_reporting_nan_or_disappearing
    // https://github.com/micrometer-metrics/micrometer-docs/issues/23 (follow commit link)
    /** map to keep references to the monitored gauges, gauge must monitor a set-able object */
    private Map<List<Tag>, AtomicInteger> projectCoverageMap = new HashMap<>();
    private Map<List<Tag>, AtomicInteger> projectOverallCoverageMap = new HashMap<>();

    @Autowired
    public CoverageMetricService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void submitCoverage(@NotNull SonarInformation sonarInformation) {
        String projectKey = sonarInformation.getProjectKey();

        Integer coverage = (int)sonarInformation.getCoverage();
        Integer maxCoverage = (int)sonarInformation.getMaxCoverage();

        Tag projectKeyTag = Tag.of("projectKey", projectKey);
        List<Tag> tags = Collections.singletonList(projectKeyTag);

        if (projectCoverageMap.containsKey(tags) && projectOverallCoverageMap.containsKey(tags)) {
            AtomicInteger coverageFromGauge = projectCoverageMap.get(tags);
            AtomicInteger overallCoverageFromGauge = projectOverallCoverageMap.get(tags);

            coverageFromGauge.set(coverage);
            overallCoverageFromGauge.set(maxCoverage);

        } else {
            AtomicInteger atomicCoverageReference = meterRegistry.gauge("gauge_coverage", tags, new AtomicInteger(coverage));
            projectCoverageMap.put(tags, atomicCoverageReference);
            AtomicInteger atomicOverallCoverageReference = meterRegistry.gauge("gauge_max_coverage", tags, new AtomicInteger(maxCoverage));
            projectOverallCoverageMap.put(tags, atomicOverallCoverageReference);
        }

    }

}
