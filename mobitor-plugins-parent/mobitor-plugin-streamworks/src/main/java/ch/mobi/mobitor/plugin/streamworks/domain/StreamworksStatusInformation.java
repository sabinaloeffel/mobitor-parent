package ch.mobi.mobitor.plugin.streamworks.domain;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState.COMPLETED;

public class StreamworksStatusInformation implements ApplicationInformation, VersionInformation {
    public static final String STREAMWORKS_STATUS = "streamworksStatus";

    private final String process;
    private final String environment;
    private String version;
    private StreamworksState state;

    public StreamworksStatusInformation(String process, String environment) {
        this.process = process;
        this.environment = environment;
    }

    @Override
    public String getType() {
        return STREAMWORKS_STATUS;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isSuccessful() {
        return state == COMPLETED;
    }

    public String getProcess() {
        return process;
    }

    public String getEnvironment() {
        return environment;
    }

    public StreamworksState getState() {
        return state;
    }

    public void setState(StreamworksState state) {
        this.state = state;
    }
}
