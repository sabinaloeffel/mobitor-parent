= Mobitor BitBucket Plugin

link:../../index.html[Back Mobitor Overview]

== Features

This plugin retrieves a deployed version on the configured environment and then detects the Git tag in BitBucket to
show the days that passed between the commit and the deployment.

The number of days is also published to actuator / prometheus.


== Configuration

The plugin requires access to your BitBucket REST API to retrieve tags and commits.

The required properties are:
[source]
mobitor.plugins.bitbucket.username=
mobitor.plugins.bitbucket.password=
mobitor.plugins.bitbucket.baseUrl=

The plugin assumes that each release that is deployed to the configured environment has a
git tag of the format: release/<version>


.screen.json
[source,json]
----
"bitBucketRepositories": [
  {
    "project": "bitBucketProjectName",
    "repository": "bitBucketRepositoryName",
    "serverName": "serverNameFromScreen",
    "applicationName": "applicationName",
    "environment": "environmentName"
  }
]
----

.Parameters
|===
| Name | Description | Required | Default

| project
| The BitBucket project name
| yes
| empty

| repository
| The BitBucket repository name (must not end with .git)
| yes
| empty

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationName
| The application name that refers to the sub-row of the server name in the same screen. Used to reference the position if this information block in the screen
| yes
| empty

| environment
| Environment of this information block. Refers to the environments in the same screen. Used to reference the position in the screen (the column).
| yes
| empty
|===

== Result

image::bitbucket-information-block.png[]

The information block shows the number of days between the commit and the deployment date.
The tooltip shows the amount of days since the commit (in case there was no deployment for some time).
