package ch.mobi.mobitor.plugin.bitbucket.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class CommitUntilDeployMetricService {

    private final MeterRegistry meterRegistry;

    private Map<List<Tag>, AtomicLong> durationCommitToDeployMap = new HashMap<>();

    @Autowired
    public CommitUntilDeployMetricService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void submitCommitUntilDeploymentDuration(@NotNull BitBucketInformation bitBucketInformation) {
        String serverName = bitBucketInformation.getServerName();
        String environment = bitBucketInformation.getEnvironment();
        long durationInDays = bitBucketInformation.getDaysSinceDeployment();

        Tag serverNameTag = Tag.of("server", serverName);
        Tag envTag = Tag.of("env", environment);
        List<Tag> tags = Arrays.asList(serverNameTag, envTag);

        if (durationCommitToDeployMap.containsKey(tags)) {
            AtomicLong atomicDuration = durationCommitToDeployMap.get(tags);
            atomicDuration.set(durationInDays);
        } else {
            AtomicLong atomicDuration = meterRegistry.gauge("gauge_commit_to_deploy", tags, new AtomicLong(durationInDays));
            durationCommitToDeployMap.put(tags, atomicDuration);
        }
    }
}
