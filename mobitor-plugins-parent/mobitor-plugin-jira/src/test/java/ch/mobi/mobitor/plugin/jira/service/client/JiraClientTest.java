package ch.mobi.mobitor.plugin.jira.service.client;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResultResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.assertj.core.api.Condition;
import org.assertj.core.api.HamcrestCondition;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.assertj.core.api.Assertions.assertThat;


public class JiraClientTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private JiraConfiguration jiraConfiguration;
    private JiraClient jiraClient;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port();
        jiraConfiguration = new JiraConfiguration();
        jiraConfiguration.setBaseUrl(baseUrl);
        jiraConfiguration.setUsername("test-username");
        jiraConfiguration.setPassword("test-password");

        jiraClient = new JiraClient(jiraConfiguration);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @Test
    public void testRetrieveFilter() {
        // arrange
        wiremock.stubFor(get(urlMatching("/rest/api/2/filter/[0-9]+"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("jira-filter-11121-response.json")
                )
        );

        String filterId = "11121";

        // act
        FilterResponse filterResponse = jiraClient.retrieveFilter(filterId);

        // assert
        assertThat(filterResponse).isNotNull();
        assertThat(filterResponse.getId()).isNotNull();
        assertThat(filterResponse.getName()).isNotNull();
        assertThat(filterResponse.getViewUrl()).isNotNull();
        assertThat(filterResponse.getSearchUrl()).isNotNull();
    }

    @Test
    public void testRetrieveFilterResult() {
        // arrange
        wiremock.stubFor(get(urlMatching("/rest/api/2/search?(.*)"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .withQueryParam("jql", WireMock.matching("(.+)"))
                                   .withBasicAuth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword())
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("jira-filter-11121-search-response.json")
                )
        );

        String searchUrl = jiraConfiguration.getBaseUrl() + "/rest/api/2/search?jql=test-jql-pseudo-query";

        // act
        FilterResultResponse filterResultResponse = jiraClient.retrieveFilterResult(searchUrl);

        // assert
        assertThat(filterResultResponse).isNotNull();
        assertThat(filterResultResponse.getTotal()).isEqualTo(7L);
        assertThat(filterResultResponse.getIssues()).isNotEmpty();
        assertThat(filterResultResponse.getIssues().get(0).getFields().getPriority().getName()).isEqualTo("Critical");

        assertThat(filterResultResponse.countCriticalIssues()).isEqualTo(1L);
        assertThat(filterResultResponse.countMajorIssues()).isEqualTo(6L);
    }

    @Test
    public void testRetrieveIssue() {
        // arrange
        wiremock.stubFor(get(urlMatching("/rest/api/2/issue/JIRA-5671"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .withBasicAuth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword())
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("jira-issue-jiraprj-5671.json")
                )
        );

        // act
        IssueResponse issueResponse = jiraClient.retrieveIssue("JIRA-5671");

        // assert
        wiremock.verify(WireMock.getRequestedFor(urlMatching("/rest/api/2/issue/(.+)")));

        assertThat(issueResponse).isNotNull();
        assertThat(issueResponse.getFields()).isNotNull();

        assertThat(issueResponse.getFields().getUpdated()).isNotNull();
        assertThat(issueResponse.getFields().getResolutiondate()).isNotNull();
        assertThat(issueResponse.getFields().getCreated()).isNotNull();
    }

    @Test
    public void testRetrieveNonExistingIssue() {
        // arrange
        wiremock.stubFor(get(urlMatching("/rest/api/2/issue/JIRA-6666"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .withBasicAuth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword())
                                   .willReturn(WireMock.aResponse().withStatus(204))
        );

        // act
        IssueResponse issueResponse = jiraClient.retrieveIssue("JIRA-6666");

        // assert
        wiremock.verify(getRequestedFor(urlMatching("/rest/api/2/issue/(.+)")));

        assertThat(issueResponse).isNull();
    }

    @Test
    public void testCreateCustomFieldJson() throws JsonProcessingException {
        JiraClient client = new JiraClient(null);

        Set<String> set = new HashSet<>();
        set.add("env-a");
        set.add("env-b");
        set.add("env-c");
        String customFieldName = "customfield_junit_000";
        String json = client.createCustomFieldsJson(customFieldName, set);

        Condition<Object> validJson = new HamcrestCondition<>(isJson());
        assertThat(json).is(validJson);

        Condition<Object> containingFields = new HamcrestCondition<>(hasJsonPath("$.fields"));
        assertThat(json).is(containingFields);

        Condition<Object> containingField = new HamcrestCondition<>(hasJsonPath("$.fields." + customFieldName));
        assertThat(json).is(containingField);

        Condition<Object> containingFieldsWithSize = new HamcrestCondition<>(hasJsonPath("$.fields." + customFieldName + ".*", IsCollectionWithSize.hasSize(set.size())));
        assertThat(json).is(containingFieldsWithSize);
    }

}
