package ch.mobi.mobitor.plugin.jira.domain;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

public class JiraInformation implements ApplicationInformation {

    public static final String JIRA = "jira";

    private long majorIssues;
    private long criticalIssues;
    private String filterId;
    private String filterName;
    private String searchUrl;
    private String viewUrl;
    private long total;
    private long maxResults;

    private boolean informationUpdated = false;

    public JiraInformation() {
    }

    @Override
    public String getType() {
        return JIRA;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    public long getMajorIssues() {
        return majorIssues;
    }

    public void setMajorIssues(long majorIssues) {
        this.majorIssues = majorIssues;
    }

    public long getCriticalIssues() {
        return criticalIssues;
    }

    public void setCriticalIssues(long criticalIssues) {
        this.criticalIssues = criticalIssues;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getSearchUrl() {
        return searchUrl;
    }

    public void setSearchUrl(String searchUrl) {
        this.searchUrl = searchUrl;
    }

    public String getViewUrl() {
        return viewUrl;
    }

    public void setViewUrl(String viewUrl) {
        this.viewUrl = viewUrl;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTotal() {
        return total;
    }

    public void setMaxResults(long maxResults) {
        this.maxResults = maxResults;
    }

    public long getMaxResults() {
        return maxResults;
    }

    public void markAsUpdated() {
        informationUpdated = true;
    }

    public boolean isInformationUpdated() {
        return informationUpdated;
    }
}
