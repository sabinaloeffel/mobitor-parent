package ch.mobi.mobitor.plugin.jira.service.client.domain;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class FilterResultResponse {

    @JsonProperty private long total;
    @JsonProperty private long maxResults;
    @JsonProperty private List<FilterResultIssues> issues;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(long maxResults) {
        this.maxResults = maxResults;
    }

    public List<FilterResultIssues> getIssues() {
        return issues;
    }

    public void setIssues(List<FilterResultIssues> issues) {
        this.issues = issues;
    }

    @JsonIgnore
    public long countCriticalIssues() {
        String priority = "Critical";

        return countPriorities(priority);
    }

    @JsonIgnore
    public long countMajorIssues() {
        String priority = "Major";

        return countPriorities(priority);
    }

    private long countPriorities(String priority) {
        long count = 0;
        if (issues != null && issues.size() > 0) {
            for (FilterResultIssues issue : issues) {
                if (priority.equals(issue.getFields().getPriority().getName())) {
                    count++;
                }
            }
        }
        return count;
    }
}
