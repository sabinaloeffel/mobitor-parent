package ch.mobi.mobitor.plugin.dwh.config;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class DwhConfig {
    private final String serverName;
    private final List<Map.Entry<String, String>> appsAndPlugins;

    public DwhConfig(String serverName, List<String> applicationNames, List<Map<String, String>> applications) {
        this.serverName = serverName;
        if (applicationNames != null) {
            if (applications != null) {
                throw illegalConfig();
            }
            this.appsAndPlugins = applicationNames.stream()
                    .map(a -> new AbstractMap.SimpleEntry<>(a, a))
                    .collect(toList());
        } else if (applications != null) {
            this.appsAndPlugins = applications.stream()
                    .map(a -> {
                        if (a.size() != 1) {
                            throw illegalConfig();
                        }
                        return a.entrySet().iterator().next();
                    })
                    .collect(toList());
        } else {
            throw illegalConfig();
        }
    }

    private RuntimeException illegalConfig() {
        return new IllegalArgumentException("Either applicationNames or applications with exactly one map entry each must be given.");
    }

    public String getServerName() {
        return serverName;
    }

    public List<Map.Entry<String, String>> getAppsAndPlugins() {
        return appsAndPlugins;
    }
}
