package ch.mobi.mobitor.plugin.dwh.service.client;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.WebClients;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

@Service
public class DwhDeploymentsService {
    private static final Logger LOG = LoggerFactory.getLogger(DwhDeploymentsService.class);

    private final DwhConfigurationService configurationService;

    @Autowired
    public DwhDeploymentsService(DwhConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Nullable
    public List<DwhDeployment> retrieveDeploymentInformation(String serverName, String plugin, String environment) {
        String url = configurationService.getDwhServerConfig(environment).getUrl() + "/versions/" + serverName;
        try {
            DwhDeployment[] dwhDeployments = WebClients.standard().get()
                    .uri(url)
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono(DwhDeployment[].class)
                    .block();
            return dwhDeployments == null
                    ? null
                    : stream(dwhDeployments)
                    .filter(d -> plugin.equals(d.getDeployObject()))
                    .collect(toList());
        } catch (Exception ex) {
            LOG.error("Could not read deployments from {}", url, ex);
        }
        return null;
    }
}
