package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityBuildConfig;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildWithVersionInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.teamCityBuilds.enabled", havingValue = "true")
public class TeamCityBuildsPlugin implements MobitorPlugin<TeamCityBuildConfig> {

    @Override
    public String getConfigPropertyName() {
        return "teamCityBuilds";
    }

    @Override
    public Class<TeamCityBuildConfig> getConfigClass() {
        return TeamCityBuildConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<TeamCityBuildConfig> configs) {
        for (TeamCityBuildConfig tcBuildConfig : configs) {
            String serverName = tcBuildConfig.getServerName();
            String applicationName = tcBuildConfig.getApplicationName();
            String environment = tcBuildConfig.getEnvironment();

            TeamCityBuildInformation tcInfo = createTeamCityBuildInformation(tcBuildConfig);
            screen.addInformation(serverName, applicationName, environment, tcInfo);
        }
    }

    private TeamCityBuildInformation createTeamCityBuildInformation(TeamCityBuildConfig buildConfig) {
        String configId = buildConfig.getConfigId();
        String label = buildConfig.getLabel();

        TeamCityBuildInformation tcInfo;
        if (buildConfig.getBuildNumberIsVersion()) {
            tcInfo = new TeamCityBuildWithVersionInformation(configId, label);
        } else {
            tcInfo = new TeamCityBuildInformation(configId, label);
        }
        tcInfo.setShowAnyBranch(buildConfig.isShowAnyBranch());

        return tcInfo;
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new TeamCityBuildsLegendGenerator().getLegendList();
    }

}
