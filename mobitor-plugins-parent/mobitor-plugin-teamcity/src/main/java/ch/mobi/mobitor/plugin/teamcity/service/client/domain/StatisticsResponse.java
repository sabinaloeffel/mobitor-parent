package ch.mobi.mobitor.plugin.teamcity.service.client.domain;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class StatisticsResponse implements Serializable {

    @JsonProperty(value = "property")
    private List<StatisticsPropertyResponse> properties;

    @JsonIgnore
    private String getProperty(String propName) {
        for (StatisticsPropertyResponse property : this.properties) {
            if (propName.equals(property.getName())) {
                return property.getValue();
            }
        }
        return null;
    }

    @JsonIgnore
    private int getPropertyAsInt(String propName) {
        String property = getProperty(propName);
        if (StringUtils.isNotEmpty(property)) {
            return Integer.parseInt(property);
        }
        return 0;
    }

    @JsonIgnore
    public int testsPassed() {
        return this.getPropertyAsInt("PassedTestCount");
    }

    @JsonIgnore
    public int testsFailed() {
        return this.getPropertyAsInt("FailedTestCount");
    }

    @JsonIgnore
    public int testsIgnored() {
        return this.getPropertyAsInt("IgnoredTestCount");
    }

    @JsonIgnore
    public int testsTotal() {
        return this.getPropertyAsInt("TotalTestCount");
    }

    @JsonIgnore
    @Nullable
    public BigDecimal coverage() {
        String lineCoverage = this.getProperty("CodeCoverageL");
        String statementCoverage = this.getProperty("CodeCoverageS");
        String coverage = lineCoverage == null ? statementCoverage : lineCoverage;
        return coverage == null ? null : new BigDecimal(coverage);
    }
}
