package ch.mobi.mobitor.plugin.teamcity.service.scheduling;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.TeamCityCoveragesPlugin;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.teamcity.service.client.TeamCityInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation.TEAM_CITY_COVERAGE;

@Component
@ConditionalOnBean(TeamCityCoveragesPlugin.class)
public class TeamCityCoverageInformationCollector {

    private static final Logger LOG = LoggerFactory.getLogger(TeamCityCoverageInformationCollector.class);

    private final ScreensModel screensModel;
    private final TeamCityInformationProviderService teamCityInformationProviderService;
    private final RuleService ruleService;
    private final CollectorMetricService collectorMetricService;

    @Autowired
    public TeamCityCoverageInformationCollector(ScreensModel screensModel,
                                                TeamCityInformationProviderService teamCityInformationProviderService, //
                                                RuleService ruleService, CollectorMetricService collectorMetricService) {

        this.screensModel = screensModel;
        this.teamCityInformationProviderService = teamCityInformationProviderService;
        this.ruleService = ruleService;
        this.collectorMetricService = collectorMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.teamCityCoverageInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectTeamCityCoverageInformationForScreens() {
        long start = System.currentTimeMillis();
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateCoverageInformation);
        long stop = System.currentTimeMillis();
        long duration = stop - start;

        LOG.info("reading TC coverage information took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("tc.build.coverage", duration);
        collectorMetricService.updateLastRunCompleted(TEAM_CITY_COVERAGE);
    }

    private void populateCoverageInformation(Screen screen) {
        List<TeamCityCoverageInformation> tcInfoList = screen.getMatchingInformation(TEAM_CITY_COVERAGE);
        tcInfoList.forEach(teamCityInformationProviderService::updateCoverageInformation);

        ruleService.updateRuleEvaluation(screen, TEAM_CITY_COVERAGE);

        screen.setRefreshDate(TEAM_CITY_COVERAGE, new Date());
    }
}
