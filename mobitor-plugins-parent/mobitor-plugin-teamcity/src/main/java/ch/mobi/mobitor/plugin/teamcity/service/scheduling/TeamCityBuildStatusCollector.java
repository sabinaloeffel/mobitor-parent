package ch.mobi.mobitor.plugin.teamcity.service.scheduling;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.TeamCityBuildsPlugin;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.service.client.TeamCityInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation.TEAM_CITY_BUILD;


@Component
@ConditionalOnBean(TeamCityBuildsPlugin.class)
public class TeamCityBuildStatusCollector {

    private static final Logger LOG = LoggerFactory.getLogger(TeamCityBuildStatusCollector.class);

    public static final String CACHE_NAME_TEAMCITY_BUILDS = "cache_teamCityBuildStatuses";
    public static final String CACHE_NAME_TEAMCITY_BUILD_STATISTICS = "cache_teamCityBuildStatistics";
    public static final String CACHE_NAME_TEAMCITY_PROJECTS = "cache_teamCityProjectStatuses";

    private final TeamCityInformationProviderService teamCityInformationProviderService;
    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final CollectorMetricService collectorMetricService;

    @Autowired
    public TeamCityBuildStatusCollector(ScreensModel screensModel,
                                        TeamCityInformationProviderService teamCityInformationProviderService,
                                        RuleService ruleService,
                                        CollectorMetricService collectorMetricService) {
        this.screensModel = screensModel;
        this.teamCityInformationProviderService = teamCityInformationProviderService;
        this.ruleService = ruleService;
        this.collectorMetricService = collectorMetricService;
    }

    // this actually works if one would not want the same polling interval throughout the day:
    // @Scheduled(cron = "0 */5 7-12 * * MON-FRI")
    // @Scheduled(cron = "0 */15 13-23 * * MON-FRI")
    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.teamCityBuildStatusInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    @CacheEvict(cacheNames = {CACHE_NAME_TEAMCITY_BUILDS, CACHE_NAME_TEAMCITY_BUILD_STATISTICS}, allEntries = true)
    public void collectTeamCityBuildInformationForScreens() {
        long start = System.currentTimeMillis();
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateBuildInformation);
        long stop = System.currentTimeMillis();
        long duration = stop - start;

        LOG.info("reading TC build status took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("tc.build.status", duration);
        collectorMetricService.updateLastRunCompleted(TEAM_CITY_BUILD);
    }

    private void populateBuildInformation(Screen screen) {
        List<TeamCityBuildInformation> tcInfoList = screen.getMatchingInformation(TEAM_CITY_BUILD);
        tcInfoList.forEach(teamCityInformationProviderService::updateBuildInformation);

        ruleService.updateRuleEvaluation(screen, TEAM_CITY_BUILD);

        screen.setRefreshDate(TEAM_CITY_BUILD, new Date());
    }

}
