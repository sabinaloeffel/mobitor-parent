package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Collections.singletonList;

public class TeamCityBuildsLegendGenerator implements MobitorPluginLegendGenerator {
    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        TeamCityBuildInformation tcb = new TeamCityBuildInformation("configId", "RC");
        tcb.setStatus("SUCCESS");
        tcb.setState("complete");

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Successful build", tcb);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        TeamCityBuildInformation tcb = new TeamCityBuildInformation("configId", "RC");
        tcb.setStatus("FAILURE");
        tcb.setState("complete");
        tcb.setTestsTotal(10);
        tcb.setTestsFailed(1);
        tcb.setTestsIgnored(1);
        tcb.setTestsPassed(8);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Failed build", tcb);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createProgressList() {
        TeamCityBuildInformation tcb = new TeamCityBuildInformation("configId", "RC");
        tcb.setStatus("SUCCESS");
        tcb.setState("running");
        tcb.setPercentageComplete(37);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Running build", tcb);
        return singletonList(wrapper);
    }

}
