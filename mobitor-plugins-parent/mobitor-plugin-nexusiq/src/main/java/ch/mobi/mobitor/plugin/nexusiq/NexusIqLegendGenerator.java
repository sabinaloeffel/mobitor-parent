package ch.mobi.mobitor.plugin.nexusiq;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public class NexusIqLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        NexusIqInformation niqi = new NexusIqInformation("publicId", Stage.BUILD);
        niqi.resetViolationsCount();
        niqi.setHasError(false);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Good NexusIQ report", niqi);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        NexusIqInformation niqViolation = new NexusIqInformation("publicId", Stage.BUILD);
        niqViolation.resetViolationsCount();
        niqViolation.setHasError(false);
        niqViolation.increaseViolationCount("policyId");

        NexusIqInformation niqErr = new NexusIqInformation("publicId", Stage.BUILD);
        niqErr.resetViolationsCount();
        niqErr.setHasError(true);

        ApplicationInformationLegendWrapper wrapperViolations = new ApplicationInformationLegendWrapper("NexusIQ report with violations", niqViolation);
        ApplicationInformationLegendWrapper wrapperErr = new ApplicationInformationLegendWrapper("NexusIQ had an error", niqErr);

        return asList(wrapperViolations, wrapperErr);
    }
}
