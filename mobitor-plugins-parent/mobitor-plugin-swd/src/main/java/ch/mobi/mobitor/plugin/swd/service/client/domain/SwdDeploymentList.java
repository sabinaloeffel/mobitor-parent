package ch.mobi.mobitor.plugin.swd.service.client.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SwdDeploymentList {

    private List<SwdDeployment> deployments;

    public SwdDeploymentList(List<SwdDeployment> deployments) {
        this.deployments = Objects.requireNonNull(deployments);
    }

    public List<SwdDeployment> getDeployments() {
        return deployments;
    }

    public Optional<SwdDeployment> findDeploymentByEnvironment(String environment) {
        Objects.requireNonNull(environment, "Environment must not be null!");
        return deployments.stream()
                          .filter(d -> environment.equals(d.getEnvironment()))
                          .findFirst();
    }

}
