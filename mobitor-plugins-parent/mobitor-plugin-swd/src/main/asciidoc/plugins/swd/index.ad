= Mobitor SWD Plugin

link:../../index.html[Back Mobitor Overview]

== Features

This plugin reads deployment information from the SWD portal and provides version information to the Mobitor.


== Configuration

.screen.json
[source, json]
----
"swdDeployments": [
    {
      "serverName": "server_name",
      "applicationName": "app_patch_name"
    }
  ]
----

.Parameters
|===
| Name | Description | Required | Default

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationName
| The application name that is shown on the row, it can be freely chosen.
| yes
| empty
|===

Environments that are queried are read from the "environments" of the same screen.


== Result

image::swd-information-block.png[]

As SWD versions are revision or commit hash based the version shown is the commit hash of the deployment.
