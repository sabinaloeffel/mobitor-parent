= Mobitor Kubernetes Plugin

link:../../index.html[Back Mobitor Overview]

== Features

Plugin that retrieves Job information from a Kubernetes Job.


== Configuration

.screen.json
[source,json]
----
"kubernetesBatchJobs": [
  {
    "labelSelector": "labelKey=labelValue",
    "namespace": "kubernetes-namespace",
    "serverName": "server_name_on_screen",
    "applicationName": "application_name_on_screen",
    "environment": "ENV"
  }
----

.Parameters
|===
| Name | Description | Required | Default

| labelSelector
| A Kubernetes label selector to find the Kubernetes Job. In key=value format.
| yes
| empty

| namespace
| The Kubernetes namespace the job is running
| yes
| empty

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationName
| The application name that refers to the sub-row of the server name in the same screen. Used to reference the position if this information block in the screen
| yes
| empty

| environment
| Environment of this information block. Refers to the environments in the same screen. Used to reference the position in the screen (the column).
| yes
| empty
|===


== Result

image::k8s-job-information-block.png[]

Shows the state of the job: yellow if it is currently running, red if it failed, green if the last
run was successful.
