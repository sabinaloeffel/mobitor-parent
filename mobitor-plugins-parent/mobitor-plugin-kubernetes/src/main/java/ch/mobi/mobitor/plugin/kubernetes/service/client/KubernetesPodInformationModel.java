package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.PodList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class KubernetesPodInformationModel {

    private Map<EnvironmentNetwork, PodList> podInformationMap = new HashMap<>();

    @Autowired
    public KubernetesPodInformationModel() {
    }

    public void addPodInformation(EnvironmentNetwork network, PodList podList) {
        podInformationMap.put(network, podList);
    }

}
