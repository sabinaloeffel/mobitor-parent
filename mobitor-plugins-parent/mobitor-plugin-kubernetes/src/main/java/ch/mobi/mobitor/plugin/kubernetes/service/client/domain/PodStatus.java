package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.TestOnly;

import java.util.LinkedList;
import java.util.List;

/**
 * @see <a href="https://kubernetes.io/docs/api-reference/v1.8/#podstatus-v1-core">PodStatus v1 core</a>
 */
public class PodStatus {

    @JsonProperty private List<ContainerStatus> containerStatuses = new LinkedList<>();

    @JsonIgnore
    public long getTotalRestartCount() {
        if (CollectionUtils.isNotEmpty(containerStatuses)) {
            long totalRestartCount = 0;
            for (ContainerStatus containerStatus : containerStatuses) {
                totalRestartCount += containerStatus.getRestartCount();
            }
            return totalRestartCount;
        }

        return -1;
    }

    @TestOnly
    void setContainerStatuses(List<ContainerStatus> containerStatuses) {
        this.containerStatuses = containerStatuses;
    }
}
