package ch.mobi.mobitor.plugin.kubernetes;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase;
import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Collections.singletonList;

public class KubernetesBatchJobLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        KubernetesBatchJobInformation jobInfo = new KubernetesBatchJobInformation("development", "app=service-name", "k8s-ns");
        jobInfo.setStatus(JobStatusPhase.SUCCEEDED);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Successful Job", jobInfo);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        KubernetesBatchJobInformation jobInfo = new KubernetesBatchJobInformation("development", "app=service-name", "k8s-ns");
        jobInfo.setStatus(JobStatusPhase.FAILED);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Failed Job", jobInfo);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createProgressList() {
        KubernetesBatchJobInformation jobInfo = new KubernetesBatchJobInformation("development", "app=service-name", "k8s-ns");
        jobInfo.setStatus(JobStatusPhase.RUNNING);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Running Job", jobInfo);
        return singletonList(wrapper);
    }
}
