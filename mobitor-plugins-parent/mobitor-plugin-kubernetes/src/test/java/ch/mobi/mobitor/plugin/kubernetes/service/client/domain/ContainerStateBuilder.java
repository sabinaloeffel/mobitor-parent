package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.time.LocalDateTime;
import java.util.function.Consumer;

public class ContainerStateBuilder {

    private ContainerStateTerminated terminated;

    private ContainerStateBuilder() {
        terminated = new ContainerStateTerminated();
    }

    public static ContainerState buildTerminatedWithDefaults() {
        return buildTerminatedWithDefaults(builder -> {});
    }

    public static ContainerState buildTerminatedWithDefaults(Consumer<ContainerStateBuilder> builderValues) {
        LocalDateTime started = LocalDateTime.of(2000, 8, 13, 15, 16, 17);
        LocalDateTime finished = started.plusSeconds(7654L);
        Consumer<ContainerStateBuilder> builderDefaults = builder -> builder.startedAt(started)
                                                                            .finishedAt(finished);
        return buildTerminated(builderDefaults.andThen(builderValues));
    }

    public static ContainerState buildTerminated(Consumer<ContainerStateBuilder> builderValues) {
        ContainerStateBuilder builder = new ContainerStateBuilder();
        builderValues.accept(builder);
        ContainerState containerState = new ContainerState();
        containerState.setTerminated(builder.buildTerminated());
        return containerState;
    }

    private ContainerStateTerminated buildTerminated() {
        return terminated;
    }

    public ContainerStateBuilder startedAt(LocalDateTime startedAt) {
        terminated.setStartedAt(startedAt);
        return this;
    }

    public ContainerStateBuilder finishedAt(LocalDateTime finishedAt) {
        terminated.setFinishedAt(finishedAt);
        return this;
    }
}
